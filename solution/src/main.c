#include <malloc.h>
#include <stdbool.h>
#include <stdio.h>

#include "bmp/bmp.h"
#include "file/file.h"
#include "image/image.h"
#include "trans/rot.h"

int main(int argc, char** argv) {
    if (argc < 3) {
        fprintf(stderr,
                "Use this program with <input_file_name> and "
                "<output_file_name> arguments\n");
        return 1;
    }
    char* input_file_name = argv[1];
    char* output_file_name = argv[2];

    FILE* fin = NULL;
    switch (open_file(&fin, input_file_name, "rb")) {
        case IO_NO_SUCH_FILE_OR_DIRECTORY:
            fprintf(stderr, "No such file or directory: %s\n", input_file_name);
            return 1;
        case IO_EMPTY_TARGET:
            fprintf(stderr, "Input file pointer broke\n");
            return 1;
        case IO_OK:
            fprintf(stdin, "Input file opened\n");
            break;
    }

    FILE* fout = NULL;
    switch (open_file(&fout, output_file_name, "wb")) {
        case IO_NO_SUCH_FILE_OR_DIRECTORY:
            fprintf(stderr, "No such file or directory: %s\n",
                    output_file_name);
            return 1;
        case IO_EMPTY_TARGET:
            fprintf(stderr, "Output file pointer broke\n");
            return 1;
        case IO_OK:
            fprintf(stdin, "Output file opened\n");
            break;
    }

    struct image image = {0};
    bool error = true;
    switch (from_bmp(fin, &image)) {
        case READ_OK:
            fprintf(stdin, "Image loaded successfully\n");
            error = false;
            break;
        case READ_INVALID_HEADER:
            fprintf(stderr, "File has an invalid header\n");
            break;
        case READ_ERROR:
            fprintf(stderr, "Read error appeared\n");
            break;
    }

    if (!error) {
        error = true;
        struct image new_image = rotate_image(image);
        switch (to_bmp(fout, &new_image)) {
            case WRITE_OK:
                fprintf(stdin, "Image saved successfully\n");
                error = false;
                break;
            case WRITE_INVALID_SOURCE:
                fprintf(stderr, "Image broke 2\n");
                break;
            case WRITE_HEADER_ERROR:
                fprintf(stderr, "Error appeared while writing new image's header\n");
                break;
            case WRITE_ERROR:
                fprintf(stderr, "Error appeared while writing new image\n");
                break;
        }
        image_destroy(&new_image);
    }

    image_destroy(&image);
    close_file(&fin);
    close_file(&fout);

    return error ? 1 : 0;
}
