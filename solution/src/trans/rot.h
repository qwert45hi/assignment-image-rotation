#ifndef ROTATION_H
#define ROTATION_H

#include <stdint.h>

#include "../image/image.h"

struct image rotate_image(struct image source);

#endif
