#include "bmp.h"

static inline size_t calculate_padding(uint64_t width) {
    return 3 - (width * 3 + 3) % 4;
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header = {0};
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1)
        return READ_INVALID_HEADER;

    *img = image_create(header.biHeight, header.biWidth);
    fseek(in, header.bOffBits, SEEK_SET);

    const size_t size_per_row = (size_t)(img->width) * sizeof(struct pixel);
    const size_t padding = calculate_padding(img->width);

    for (uint64_t i = 0; i < img->height; i++) {
        if (fread(img->data + i * (img->width), size_per_row, 1, in) != 1 ||
            fseek(in, padding, SEEK_CUR))
            return READ_ERROR;
    }

    return READ_OK;
}

static inline struct bmp_header create_bmp_header(uint64_t width, uint64_t height, uint64_t image_size) {
    return (struct bmp_header) {
        .bfType = TYPE,
        .bfSize = image_size + sizeof(struct bmp_header),
        .bfReserved = RESERVED,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = SIZE,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = PLANES,
        .biBitCount = BIT_COUNT,
        .biCompression = COMPRESSION,
        .biSizeImage = image_size,
        .biXPelsPerMeter = PIXELS_PER_METER,
        .biYPelsPerMeter = PIXELS_PER_METER,
        .biClrUsed = CLR_USED,
        .biClrImportant = CLR_IMPORTANT,
    };
}

enum write_status to_bmp(FILE* out, struct image* img) {
    if (img->width == 0 || img->height == 0 || img->data == NULL)
        return WRITE_INVALID_SOURCE;

    const size_t padding = calculate_padding(
        img->width);
    const size_t size_per_row = img->width * sizeof(struct pixel);

    const struct bmp_header header = create_bmp_header(img->width, img->height, (size_per_row + padding) * img->height);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1 ||
        fseek(out, header.bOffBits, SEEK_SET))
        return WRITE_HEADER_ERROR;

    uint8_t* padding_bytes = malloc(padding);
    for (size_t i = 0; i < padding; i++) padding_bytes[i] = 0;

    for (uint64_t i = 0; i < img->height; i++) {
        if (fwrite(img->data + i * img->width, size_per_row, 1, out) != 1 ||
            fwrite(padding_bytes, padding, 1, out) != 1) {
            free(padding_bytes);
            return WRITE_ERROR;
        }
    }

    free(padding_bytes);
    return WRITE_OK;
}
